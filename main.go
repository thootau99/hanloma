package main

import (
	"log"
	"net/http"
	"os"
	"strings"
	"unicode"

	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
)

var Lipiau map[string]string // tha̍k lī piáu li̍p lâi
type HanliRequest struct {
	Hanli string `json:"hanli"`
}

func checkHan(li string) bool {
	result := false
	for _, runeValue := range li {
		if unicode.Is(unicode.Han, runeValue) {
			result = true
			break
		}
	}

	return result
}

func chhuli(path string) map[string]string {
	hanli := [][]string{}
	lomali := make(map[string]string)
	result := make(map[string]string)

	file, _ := os.ReadFile(path)
	split := strings.Split(string(file), "\n")
	for index := range split {
		object := strings.Split(split[index], "\t")
		if checkHan(object[0]) {
			hanli = append(hanli, object)
		} else {
			lomali[object[1]] = object[0]
		}
	}

	for index := range hanli {
		key := string(hanli[index][0])
		if _, exist := result[key]; !exist {
			result[key] = lomali[hanli[index][1]]
		}
	}

	return result
}

func choanoann(hanli string) string {
	split := strings.Split(hanli, "")
	result := []string{}
	addSpace := false
	for index := range split {
		if checkHan(split[index]) {
			POJ := Lipiau[split[index]] + " "
			result = append(result, POJ)
			addSpace = true
		} else if split[index] == " " {
			if addSpace == true {
				addSpace = false
				continue
			} else {
				result = append(result, split[index])
			}
		} else {
			result = append(result, split[index])
			addSpace = false

		}
	}
	return strings.Join(result, "")
}

func main() {
	Lipiau = chhuli("./dict/taigi.dict.yaml")

	router := gin.Default()
	router.POST("/choanoann", func(context *gin.Context) {
		result := make(map[string]string)
		var hanli HanliRequest
		if err := context.ShouldBindBodyWith(&hanli, binding.JSON); err != nil {
			log.Printf("%+v", err)
		}
		result["kiatko"] = choanoann(hanli.Hanli)
		context.JSON(http.StatusOK, result)
	})

	router.Run(":3333")
}
